import {createStore} from "redux";

const store = createStore(getToken);

function getToken(state=[], action) {
    if(localStorage.getItem("token")){
        return state = [{token: localStorage.getItem("token")}]
    }
    if(action.value === undefined || action.value === ''){
        return state=[]
    }else if(action.type === "token") {
        localStorage.setItem('token', action.value.token);
        return [
            action.value
        ];
    }
    return state
}

store.subscribe(() => {
    console.log(store.getState())
});

export default store
