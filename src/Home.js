import React from 'react';
import './style.scss';
import ProductList from "./component/ProductList";
import {Switch, Route,} from "react-router-dom";
import ProductDetails from "./component/ProductDetails";
import RegisterView from "./component/RegisterView";
import LoginView from "./component/LoginView";
import Navigation from "./component/Navigation";

export default class Home extends React.Component {
    render() {
    return(
        <div className={"container"}>
            <Navigation />
            <Switch>
                <Route exact path='/' component={ProductList}/>
                <Route exact path='/products' component={ProductList}/>
                <Route path='/products/:id' component={ProductDetails}/>
                <Route path='/register' component={RegisterView}/>
                <Route path='/login' component={LoginView}/>
            </Switch>
        </div>
    )}
  }

