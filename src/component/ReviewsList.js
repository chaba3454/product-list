import React, {Component} from 'react';
import axios from "axios";
import SingleReview from "./SingleReview";

class ReviewsList extends Component {
    constructor(props){
        super(props);
        this.state = {
            comments: [],
            id: this.props.product_id,
        };
        this.getComments();
    };

    async getComments(){
        const response =
            await axios.get("https://smktesting.herokuapp.com/api/reviews/" + `${this.state.id}` );
        this.setState({
            comments: response.data.reverse()
        });
        console.log(response.data)
    }

    render() {
        return (
                <div className='review_list'>
                    <h1>Reviews: </h1>
                    {this.state.comments.map((comment) => {
                    return (
                        <ul>
                            <li key={comment.id}>
                                <SingleReview single_prop={comment} />
                            </li>
                        </ul>
                    )})}
                </div>
        );
    }
}

export default ReviewsList;
