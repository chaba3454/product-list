import React, {Component} from 'react';
import axios from "axios";
import store from "./Store";

class LoginView extends Component {
    constructor(props){
        super(props);
        this.state ={
            username : "",
            password: "",
            underlineText: ''
        }
    }

    handleClick(e) {
        if(this.state.username === '' || this.state.password === ''){
            this.setState({
                underlineText: "Username or password is empty"
            });
            return
        }

        var apiUrl = "https://smktesting.herokuapp.com/api/login/";
        var payload = {
            "username": this.state.username,
            "password": this.state.password
        };
        axios
            .post(apiUrl, payload)
            .then((response) => {
                if(response.data.token){
                    store.dispatch ({
                        type: "token",
                        value: {token: response.data.token}
                    });
                }else if(response.data.success === false) {
                    this.setState({
                        underlineText: response.data.message
                    })
                } else {
                    store.dispatch ({
                        type: "token",
                        value: response.data.token
                    });
                }
                if(store.getState()[0]){
                    this.props.history.push('/products');
                }
                console.log(response.data);
            }, (error) => {
                console.log(error);
            });
    }

    render() {
        return (
            <div className='login_register_view'>
                <div>
                <p>Username: <input type='text' onChange={
                    (e) => this.setState({username: e.target.value})}/>
                </p>
                <p>Password: <input type='password' onChange={
                    (e) => this.setState({password: e.target.value})}/></p>
                {this.state.underlineText}
                <button className='btn btn-sm btn-success' onClick={(e) => this.handleClick(e)}>Submit</button>
                </div>
            </div>
        );
    }
}

export default LoginView;
