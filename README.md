### Product List
- This is Product List test task made on React with Redux and axios.
- You can check how this works here: http://product-list-chaba.herokuapp.com/

### Clone your project
- Go to your computer's shell and type the following command with your SSH:
- `$ git clone git@gitlab.com:chaba3454/product-list.git`
- Use `cd product-list` to change directory to your project.
- Then `npm install` to install dependencies

### `npm run dev` to start project on your local machine.

### Dependencies
- npm v6.13.2
- node v8.10.0
