import React, {Component} from 'react';

class SingleReview extends Component {
    render() {
        return (
            <div className='single_review'>
                <div>User: {this.props.single_prop.created_by.username} <p>at {this.props.single_prop.created_at}</p></div>
                <div>Rate: {this.props.single_prop.rate}</div>
                <div>Text: {this.props.single_prop.text}</div>
            </div>
        );
    }
}

export default SingleReview;
