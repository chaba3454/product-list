import React, {Component} from 'react';

class SingleProduct extends Component {
    render() {
        //Fix problem with /static/undefined
        if (this.props.product_prop.id) {
            return(
                <div className='single_product text-center'>
                    <img className="img-thumbnail" src={'https://smktesting.herokuapp.com/static/' + this.props.product_prop.img} alt=' '/>
                    <h2>{this.props.product_prop.title}</h2>
                    <hr/>
                    <h4>{this.props.product_prop.text}</h4>
                    <hr/>
                </div>
            )
        } else {
            return (<div></div>)
        }
    }
}

export default SingleProduct;
