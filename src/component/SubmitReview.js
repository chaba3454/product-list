import React, {Component} from 'react';
import axios from "axios";
import store from "./Store";
import StarRatingComponent from "react-star-rating-component";

class SubmitReview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            rating: 5,
            underCommentText: ""
        };
    }

    onStarClick(nextValue, prevValue, name) {
        this.setState({rating: nextValue});
    }

    handlerAddComment(e) {
        if(this.state.text === ''){
            this.setState({
                underCommentText: "Field is empty"
            });
            return
        }

        var baseUrl = "https://smktesting.herokuapp.com/api/reviews/" + this.props.product_prop;
        var data = {
            "text": this.state.text,
            "rate": this.state.rating
        };
        var headers = {
            headers: {
                "Authorization": "Token " + store.getState()[0].token
            }
        };
        axios
            .post(baseUrl, data, headers)
            .then((response) => {
                console.log(response)
            });
        this.setState({
            text: '',
            underCommentText: '',
            rating: 5
        });
    }

    render() {
        if(store.getState()[0]){
            return (
                <div className='submit_review text-center'>
                    <p>Please leave a comment below </p>
                    <div className='star_rating'>
                        <StarRatingComponent
                        name="rate1"
                        starCount={5}
                        value={this.state.rating}
                        onStarClick={this.onStarClick.bind(this)}
                    />
                    <input type={"textarea"} placeholder='Type your review'
                           value={this.state.text}
                           onChange={(e) => {this.setState({text: e.target.value})}}/>
                    <button className='btn btn-sm btn-danger' onClick={(e) => {this.handlerAddComment(e)}}>Add</button>
                    </div>
                    <p>{this.state.underCommentText}</p>
                </div>
            );
        } else {
            return (
                <div className='text-center text-black-50'>
                    <p>Only authorized persons can leave a comment.</p>
                    <p>Please login or register!</p>
                </div>
            )
        }
    }
}

export default SubmitReview;
