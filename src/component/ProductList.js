import React from "react";
import axios from "axios";
import SingleProduct from "./SingleProduct";
import {Link} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

export default class ProductList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
        };
        this.getProductList();
    }

    async getProductList(){
        const response =
            await axios.get("https://smktesting.herokuapp.com/api/products/");
        this.setState({
            products: response.data
        });
        console.log(response.data)
    }

    render() {
        return(
        <div className='product_list'>
            <div className='jumbotron text-center'>
                <h1>Product List</h1>
                <p className='text-black-50'>Le Lorem Ipsum est simplement du faux texte employé dans la composition et
                    la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis
                    les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un
                    livre spécimen de polices de texte...</p>
            </div>
            <div className='d-flex'>
                {this.state.products.map(product => {
                if(product.img.length > 4 ) {
                        return (
                               <ul className='col-6 col-md-4 col-lg-3 justify-content-center'>
                                   <li className='col-11' key={product.id}>
                                       <SingleProduct product_prop={product}/>
                                       <Link to={`/products/${product.id}`}><button className="btn btn-sm btn-secondary"
                                       >Details</button></Link>
                                   </li>
                               </ul>
                        )
                    }
                })}
            </div>
        </div>
    )}

}

