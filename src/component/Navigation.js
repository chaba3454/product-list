import React, {Component} from 'react';
import {Link} from "react-router-dom";
import store from "./Store";
import '../style.scss'
import 'bootstrap/dist/css/bootstrap.min.css';

class Navigation extends Component {
    constructor(props){
        super(props);
        store.subscribe(() => {
            this.forceUpdate()
        });
    }

    Logout(){
        localStorage.clear();
        store.dispatch({
            type: "token",
            value: ''
        })
    }

    render() {
        if(store.getState()[0]){
            return (
                <div className='navigation'>
                        <nav-brand><Link to='/'>Chaba</Link></nav-brand>
                        <ul>
                            <li>
                                <Link to="/products">Products</Link>
                            </li>
                            <li>
                                <Link to='/'><button className='btn btn-sm btn-success'
                                    onClick={this.Logout.bind(this)}>Logout</button></Link>
                            </li>
                        </ul>
                </div>
            );
        } else {
            return (
                <div className='navigation'>
                        <nav-brand><Link to='/'>Chaba</Link></nav-brand>
                        <ul>
                            <li>
                                <Link to="/products">Products</Link>
                            </li>
                            <li>
                                <Link to="/register">Register</Link>
                            </li>
                            <li>
                            <Link to="/login">Login</Link>
                            </li>
                        </ul>
                </div>
            )
        }
    }
}

export default Navigation;
