import React, {Component} from 'react';
import SingleProduct from "./SingleProduct";
import ReviewsList from "./ReviewsList";
import axios from "axios";
import {Link} from "react-router-dom";
import SubmitReview from "./SubmitReview";

class ProductDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: {},
            id: +this.props.match.params.id
        };
        this.getProduct();
    }

    async getProduct(){
        const response =
            await axios.get("https://smktesting.herokuapp.com/api/products/");
        this.setState({
            product: response.data.find(x => x.id === this.state.id)
        });
    }

    render() {
        return (
            <div className='product_details d-flex'>
                <div className='col-7 inner_single_product_and_submit'>
                    <Link to="/"><button className='btn btn-sm btn-primary'>Back</button></Link>
                    <SingleProduct product_prop={this.state.product}/>
                    <SubmitReview product_prop={this.state.id} />
                </div>
                <div className='col-5 inner_review_list'>
                    <ReviewsList product_id={this.state.id}/>
                </div>
            </div>
        );
    }
}

export default ProductDetails;
